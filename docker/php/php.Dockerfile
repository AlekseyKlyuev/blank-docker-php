FROM php:8-fpm

RUN apt-get update && apt-get install -y \
        curl \
        wget \
        git \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        libfreetype6-dev \
        zlib1g-dev \
        libxml2-dev \
        libzip-dev \
        libpq-dev \
        libonig-dev \
        graphviz \
        libssl-dev \
    && docker-php-ext-install -j$(nproc) iconv mbstring pdo_pgsql zip \
    && docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd sockets pcntl

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN groupadd -g 1000 project
RUN useradd -u 1000 -ms /bin/bash -g project project

# Set working directory
WORKDIR /var/www
RUN chown -R project /var/www

USER project
